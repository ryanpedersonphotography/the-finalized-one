<?php
// Register Custom Post Type
function custom_casestudy()
{

    $labels = array(
    'name'                  => 'Case Studies',
    'singular_name'         => 'Case Study',
    'menu_name'             => 'Case Studies',
    'name_admin_bar'        => 'Case Study',
    'archives'              => 'Case Study Archives',
    'parent_item_colon'     => 'Parent Case Study:',
    'all_items'             => 'All Case Studies',
    'add_new_item'          => 'Add New Case Study',
    'add_new'               => 'Add New Case Study',
    'new_item'              => 'New Case Study',
    'edit_item'             => 'Edit Case Study',
    'update_item'           => 'Update Case Study',
    'view_item'             => 'View Case Study',
    'search_items'          => 'Search Case Studies',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Case Studies list',
    'items_list_navigation' => 'Case Studies list navigation',
    'filter_items_list'     => 'Filter Case Study list',
    );
    $args = array(
    'label'                 => 'Case Study',
    'description'           => 'Case Studies',
    'labels'                => $labels,
    'supports'              => array( 'title', 'author', 'thumbnail', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-format-aside',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'            => array( 'slug' => 'work' ),
    'capability_type'       => 'page',
    );
    register_post_type('casestudy', $args);

}
add_action('init', 'custom_casestudy', 0);

<?php

// Register Author Custom Post Type
function cehd_register_author_cpt() {

	$labels = array(
		'name'                  => _x('Authors', 'Post Type General Name', 'cehd'),
		'singular_name'         => _x('Author', 'Post Type Singular Name', 'cehd'),
		'menu_name'             => __('Authors', 'cehd'),
		'name_admin_bar'        => __('Authors', 'cehd'),
		'archives'              => __('Author Archives', 'cehd'),
		'parent_item_colon'     => __('', 'cehd'),
		'all_items'             => __('All Authors', 'cehd'),
		'add_new_item'          => __('Add New Author', 'cehd'),
		'add_new'               => __('Add New', 'cehd'),
		'new_item'              => __('New Author', 'cehd'),
		'edit_item'             => __('Edit Author', 'cehd'),
		'update_item'           => __('Update Author', 'cehd'),
		'view_item'             => __('View Author', 'cehd'),
		'search_items'          => __('Search Author', 'cehd'),
		'not_found'             => __('Not found', 'cehd'),
		'not_found_in_trash'    => __('Not found in Trash', 'cehd'),
		'featured_image'        => __('Featured Image', 'cehd'),
		'set_featured_image'    => __('Set featured image', 'cehd'),
		'remove_featured_image' => __('Remove featured image', 'cehd'),
		'use_featured_image'    => __('Use as featured image', 'cehd'),
		'insert_into_item'      => __('Insert into item', 'cehd'),
		'uploaded_to_this_item' => __('Uploaded to this item', 'cehd'),
		'items_list'            => __('Authors list', 'cehd'),
		'items_list_navigation' => __('Authors list navigation', 'cehd'),
		'filter_items_list'     => __('Filter authors list', 'cehd'),
	);
	$rewrite = array(
		'slug'                  => 'authors',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => __('Author', 'cehd'),
		'description'           => __('Authors post type for people index, categorizing and bio pages', 'cehd'),
		'labels'                => $labels,
		'supports'              => array('title', 'editor', 'thumbnail', 'revisions', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => 'authors',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
	);
	register_post_type('author', $args );

}
add_action('init', 'cehd_register_author_cpt', 0);



// Register Custom Taxonomy
function cehd_register_author_tax() {

	$labels = array(
		'name'                       => _x( 'Author Categories', 'Taxonomy General Name', 'cehd' ),
		'singular_name'              => _x( 'Author Category', 'Taxonomy Singular Name', 'cehd' ),
		'menu_name'                  => __( 'Categories', 'cehd' ),
		'all_items'                  => __( 'All Items', 'cehd' ),
		'parent_item'                => __( 'Parent Item', 'cehd' ),
		'parent_item_colon'          => __( 'Parent Item:', 'cehd' ),
		'new_item_name'              => __( 'New Category Name', 'cehd' ),
		'add_new_item'               => __( 'Add New Category', 'cehd' ),
		'edit_item'                  => __( 'Edit Category', 'cehd' ),
		'update_item'                => __( 'Update Category', 'cehd' ),
		'view_item'                  => __( 'View Category', 'cehd' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'cehd' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'cehd' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'cehd' ),
		'popular_items'              => __( 'Popular Categories', 'cehd' ),
		'search_items'               => __( 'Search Categories', 'cehd' ),
		'not_found'                  => __( 'Not Found', 'cehd' ),
		'no_terms'                   => __( 'No categories', 'cehd' ),
		'items_list'                 => __( 'Categories list', 'cehd' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'cehd' ),
	);
	$rewrite = array(
		'slug'                       => 'authors/categories',
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'author_category', array( 'author' ), $args );

}
add_action( 'init', 'cehd_register_author_tax', 0 );

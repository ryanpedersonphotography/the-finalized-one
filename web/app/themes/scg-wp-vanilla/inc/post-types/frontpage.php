<?php
// Register Custom Post Type
function custom_casestudy()
{

    $labels = array(
    'name'                  => 'Front Page',
    'singular_name'         => 'Front Page',
    'menu_name'             => 'Front Page',
    'name_admin_bar'        => 'Front Page',
    'archives'              => 'Front Page Archives',
    'parent_item_colon'     => 'Parent Front Page:',
    'all_items'             => 'All Front Page',
    'add_new_item'          => 'Add New Front Page',
    'add_new'               => 'Add New Front Page',
    'new_item'              => 'New Front Page',
    'edit_item'             => 'Edit Front Page',
    'update_item'           => 'Update Front Page',
    'view_item'             => 'View Front Page',
    'search_items'          => 'Search Front Page',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Front Page list',
    'items_list_navigation' => 'Front Page list navigation',
    'filter_items_list'     => 'Filter Front Page list',
    );
    $args = array(
    'label'                 => 'Front Page',
    'description'           => 'Front Page',
    'labels'                => $labels,
    'supports'              => array( 'title', 'author', 'thumbnail', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-format-aside',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'            => array( 'slug' => 'work' ),
    'capability_type'       => 'page',
    );
    register_post_type('casestudy', $args);

}
add_action('init', 'custom_casestudy', 0);

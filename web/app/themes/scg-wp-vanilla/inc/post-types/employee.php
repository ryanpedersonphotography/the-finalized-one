<?php
// Register Custom Post Type
function custom_employee()
{

    $labels = array(
    'name'                  => 'Employee',
    'singular_name'         => 'Employee',
    'menu_name'             => 'Employees',
    'name_admin_bar'        => 'Employee',
    'archives'              => 'Employee Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => 'All Employees',
    'add_new_item'          => 'Add New Employee',
    'add_new'               => 'Add New Employee',
    'new_item'              => 'New Employee',
    'edit_item'             => 'Edit Employee',
    'update_item'           => 'Update Employee',
    'view_item'             => 'View Empoyee',
    'search_items'          => 'Search Employees',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Employee list',
    'items_list_navigation' => 'Employee list navigation',
    'filter_items_list'     => 'Filter items list',
    );
    $args = array(
    'label'                 => 'Employee',
    'description'           => 'Employees',
    'labels'                => $labels,
    'supports'              => array( 'title', 'thumbnail', ),
    'hierarchical'          => true,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-format-aside',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => array( 'slug' => 'employees' ),
    'capability_type'       => 'page',
    );
    register_post_type('employee', $args);

}
add_action('init', 'custom_employee', 0);

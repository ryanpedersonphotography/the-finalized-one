<?php

////////////////////////////////////////////////
// SETUP
////////////////////////////////////////////////

require_once 'inc/post-types/casestudy.php';
require_once 'inc/post-types/employee.php';
// add_theme_support('post-thumbnails');
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
// add_image_size( 'post-thumbnails', 220, 180 );
add_theme_support('soil-clean-up');
add_theme_support('soil-jquery-cdn');
add_theme_support('soil-js-to-footer');
add_theme_support('soil-google-analytics', 'UA-3570631-1', 'wp_head');

function custom_mtypes( $m ){
    $m['svg'] = 'image/svg+xml';
    $m['svgz'] = 'image/svg+xml';
    return $m;
}
add_filter( 'upload_mimes', 'custom_mtypes' );


function my_js_variables(){
$left_video_file = get_field( 'left_video_file','options' );
?>

      <script type="text/javascript">
        var frontPage = '<?php if (is_front_page()) { echo "frontpage"; } elseif (is_singular('casestudy')) { echo "casestudy"; } else { }   ?>';
        var pageID = '<?php echo get_the_ID(); ?>';
        var pageID = '<?php echo get_the_ID(); ?>';        
      </script><?php
}
add_action ( 'wp_head', 'my_js_variables' );




function sv_add_button_shortcode( $atts, $content = null ) {

  $a = shortcode_atts( array(
        'url'       => '#',
        'target'    => '_self',
    ), $atts );
    return '<a href="'. esc_attr($a['url']) .'"  target="'. esc_attr($a['target']) .'" class="sv-button">'. $content .'</a>';

}
add_shortcode( 'sv_button', 'sv_add_button_shortcode' );

function random_picture($atts) {

return '<img src="http://lorempixel.com/'. $width . '/'. $height . '" />';
}



function scg_styles()
{
		wp_register_style('owl-css', get_template_directory_uri() . '/css/owl.carousel.min.css', false, NULL);
		wp_enqueue_style('owl-css');

		wp_register_style('owl-css-theme', get_template_directory_uri() . '/css/owl.theme.default.css', false, NULL);
		wp_enqueue_style('owl-css-theme');

    wp_register_style('app-css', get_template_directory_uri() . '/css/app.css', false, null);
    wp_enqueue_style('app-css');

		wp_register_style('animate-css', get_template_directory_uri() . '/bower_components/animate.css/animate.min.css', false, null);
    wp_enqueue_style('animate-css');    wp_register_style('animate-css', get_template_directory_uri() . '/bower_components/animate.css/animate.min.css', false, null);

}
add_action('wp_enqueue_scripts', 'scg_styles');


function scg_scripts()
{
    // wp_deregister_script('jquery');

    wp_register_script('owl-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), null, true);
    wp_enqueue_script('owl-js');

    wp_register_script('app-js', get_template_directory_uri() . '/js/app.js', array('jquery', 'ajax-load-more'), null, true);
    wp_enqueue_script('app-js');


    $translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() ); // passing template URL path to custom javascript
    wp_localize_script( 'app-js', 'object_name', $translation_array ); // passing template URL path to custom javascript

}
add_action('wp_enqueue_scripts', 'scg_scripts', 999);



//////////////////
// MENU SUPPORT
////////////////
function scg_menu() {
  register_nav_menus(
    array(
      'scg-menu-left' => __( 'SCG Menu Left' ),
      'scg-menu-right' => __( 'SCG Menu Right' ),
      'scg-menu-mobile' => __( 'SCG Menu Mobile' ),
    )
  );
}
add_action( 'init', 'scg_menu' );

////////////////////////////////////////////////
// CUSTOM POST TYPES
////////////////////////////////////////////////




if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Front Page Settings',
		'menu_title'	=> 'Front Page',
		'menu_slug' 	=> 'front-page-settings',
		'capability'	=> 'administrator',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'About Us Page New',
		'menu_title'	=> 'About Us Page New',
		'menu_slug' 	=> 'option-about-us-settings',
		'capability'	=> 'administrator',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Our Philosophy Page Settings',
		'menu_title'	=> 'Our Philosophy Page',
		'menu_slug' 	=> 'option-our-philosophy-settings',
		'capability'	=> 'administrator',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));



	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

}


function limit_words($string, $word_limit) {

	// creates an array of words from $string (this will be our excerpt)
	// explode divides the excerpt up by using a space character

	$words = explode(' ', $string);

	// this next bit chops the $words array and sticks it back together
	// starting at the first word '0' and ending at the $word_limit
	// the $word_limit which is passed in the function will be the number
	// of words we want to use
	// implode glues the chopped up array back together using a space character

	return implode(' ', array_slice($words, 0, $word_limit));

}

// see this: https://code.tutsplus.com/tutorials/function-examination-wp_nav_menu--wp-25525 for cool stuff
// basically taken from here: https://galengidman.com/2014/05/08/adding-static-menu-items-to-wp_nav_menu/

function my_nav_wrap() {
  // default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'

  // open the <ul>, set 'menu_class' and 'menu_id' values
  $wrap  = '<ul id="got-gridbox %1$s" class="row %2$s">';

  // get nav items as configured in /wp-admin/
  $wrap .= '%3$s';

  // the static link
  $wrap .= '<li class="logo-position"><a href="/"><img src="'.get_template_directory_uri().'/images/SCG.svg" alt="scg"></a></li>';

  // close the <ul>
  $wrap .= '</ul>';

  // return the result
  return $wrap;
}

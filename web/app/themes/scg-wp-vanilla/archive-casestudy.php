<?php
/*
 Template Name: View All Case Studies
*/
?>
<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
<!-- <section class="hero" style="background: url('<?php
  $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
  if  (! empty($featured_image_url) ) :
    the_post_thumbnail_url('full');
  else:
     echo "https://dummyimage.com/2000x1200/c2c0c2/575757.png&text=our+work";
  endif;
?>') no-repeat center center;  background-size: cover;">
<div class="row intro expanded">

</div>
</section> -->

<section class="section text-center">
		<br />
		<br />
		<br />
	<h2 class="animated fadeInRight">Our Work</h2>
		<br />
	  	<div class="row material-faster-card-container">
			<!-- card group container -->
			<div class="material-faster-card">
				<a href="https://www.scgpr.com/our-work/cehd-case-study/">
					<div class="material-faster-card-image" style="background-image: url(https://www.scgpr.com/wp-content/uploads/2016/10/ourwork-cehd.png);">
						<div class="material-faster-card-hide">Learn More!</div>
					</div>
					<div class="material-faster-card-content">Going Global with University of Minnesota CEHD</div>
				</a>
			</div>
			<div class="material-faster-card">
				<a href="https://www.scgpr.com/our-work/rocket-league-case-study/">
					<div class="material-faster-card-image" style="background-image: url(https://www.scgpr.com/wp-content/uploads/2016/10/ourwork-rocketleague.png);">
						<div class="material-faster-card-hide">Learn More!</div>
					</div>
					<div class="material-faster-card-content">Revitalizing Rocket League’s Website</div>
				</a>
			</div>
			<div class="material-faster-card">
				<a href="http://www.scgpr.com/our-work/ratelinx-case-study/">
					<div class="material-faster-card-image" style="background-image: url(https://www.scgpr.com/wp-content/uploads/2016/10/ourwork-ratelinx.png);">
				   		<div class="material-faster-card-hide">Learn More!</div>
				    </div>
				    <div class="material-faster-card-content">Creating a Brand Identity for RateLinx</div>
				 </a>
			</div>
		</div>
</section>
<br /><br /><br />

<?php endwhile; ?>

<?php get_footer(); ?>

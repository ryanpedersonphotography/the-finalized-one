<?php
/*
 Template Name: Ryan's Spark Simplify Soar! Section Page Template
*/
?>
<?php get_header(); ?>
<style>
.menu-icon::after {
    background:  #212121!important;
    box-shadow: 0 7px 0 #212121, 0 14px 0 #212121 !important;
}
</style>
<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START INTRO HEADLINE
//////////////////////////////
////////////////////////////// -->



<section class="hero" style="background: url('<?php the_field( 'hero_banner_image' ); ?>') no-repeat center center;  background-size: cover;">
  <div class="row intro expanded">
    <!-- <div class="columns">
      <h1><?php the_title(); ?></h1>
      <p>Going Global with University of Minnesota CEHD</p>
    </div> -->
    <!-- <div class="small-centered medium-uncentered medium-6 large-5 columns">
      <div class="tech-img"></div>
    </div> -->
  </div>
</section>

<section class="section section-case-study-single">
    <div class="row">
      <div class="small-12 large-12 columns">
          <div class="row column text-center align-center"><hr class="dotted"></div>
            <h1 class="text-center">
                <?php the_title(); ?>
            </h1>
            <div class="text-center">
                <h4 class"text-center">
                    <!-- <?php the_field('sub_headline_text', 'option'); ?> -->
                </h4>
            </div>
            <div class="row column"><hr class="dotted"></div>
        </div>
    </div>
</section>


 <?php if ( have_rows( 'page_section' ) ): ?>
 	<?php while ( have_rows( 'page_section' ) ) : the_row(); ?>
 		<?php // warning: layout '' has no sub fields ?>

            <section class="section section-page-header-case-study"  style="height:5rem!important;">
                <div class="row collapse expanded text-center">
                    <div class="medium-12 columns">
                        <div class="page-header-billboard">
                            <div class="inner">
                              <h2 style="font-size:40px!important; margin-top:7px!important;"><?php the_sub_field( 'section_title' ); ?></h2>
                            </div>
                      <img src="<?php echo get_template_directory_uri(); ?>/images/banner-background-<?php the_sub_field( 'section_title_background_color' ); ?>.svg">
                      <!-- <div class="overlay" style="opacity:.5!important;"></div> -->
                  </div>
              </div>
          </div>
      </section>
      <?php if (the_sub_field( 'section_layout_style' )=='left-layout'): ?>
      <section class="section-case-study section-case-study-single">
          <div class="row column"><hr class="dotted" style="margin-top:20px!important;"></div>
          <div class="row">
              <div class="small-12 medium-6 columns align-middle text-center">
                  <div class="row small-12 medium-6 text-center align-center">
                      <div class="column small-12 medium-12">
                          <blockquote class="cite-left">
                            <?php if ( get_field( 'spark_image','option') ) { ?>
                            	<img src="<?php the_sub_field( 'section_image' ); ?>" style="border-radius: 50%!important; width:60%;"  />
                            <?php } ?>
                            <!-- <p>
                            <?php the_sub_field( 'section_quote_text' ); ?>
                            </p> -->
                        </blockquote>
                    </div>
                  </div>
              </div>
            <div class="small-12 medium-6 columns align-middle">
              <p class="blog-text text-left">
                <?php the_sub_field( 'section_text' ); ?>
              </p>
            </div>
          </div>
          <div class="row column"><hr class="dotted"></div>
      </section>
    <?php elseif (the_sub_field( 'section_layout_style' )=='right-layout'): ?>
        <section class="section-case-study section-case-study-single">
            <div class="row column"><hr class="dotted"></div>
            <div class="row">
              <div class="small-12 medium-6 columns align-middle">
                  <p class="blog-text text-left">
                    <?php the_sub_field( 'section_text' ); ?>
                  </p>
              </div>
              <div class="small-12 medium-6 columns align-middle text-center">
                  <div class="row small-12 medium-6 text-center align-center">
                      <div class="column small-12 medium-12">
                        <blockquote class="cite">
                            <img src="<?php the_sub_field( 'section_image' ); ?>" style="border-radius: 50%!important; width:60%;"  />
                            <?php the_sub_field( 'section_quote_text' ); ?>
                        </blockquote>
                    </div>
                  </div>
              </div>
            </div>
            <div class="row column"><hr class="dotted"></div>
        </section>
      <?php else: ?>
        <section class="section-case-study section-case-study-single">
            <!-- <div class="row column"><hr class="dotted"></div>
            <div class="row column"><hr class="dotted"></div> -->

            <div class="row">
              <div class="small-12 medium-12 columns align-middle text-center">
                  <p class="blog-text">
                        <?php the_field('capabilities_text','option'); ?>
                  </p>
              </div>
            </div>
        </section>
        <?php if ( have_rows( 'info_balls' ) ): ?>
  				<?php while ( have_rows( 'info_balls' ) ) : the_row(); ?>
        <div class="row small-up-1 medium-up-4">
                   <div class="columns">
                     <div class="container" style="box-sizing: border-box!important;">
                       <div class="circle kitten <?php the_sub_field( 'info_ball_color' ); ?>" style="box-sizing: border-box!important;">
                         <div class="aligner" style="box-sizing: border-box!important;">
                           <h2 style="line-height:1em;font-size:2em;color:white;"><span style="text-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);"><?php the_sub_field( 'info_ball_big_text' ); ?></span></h2>
                           <h5 style="line-height:1em;font-size:2em;color:white;"><span style="text-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);"><?php the_sub_field( 'info_ball_small_text' ); ?></span></h5>
                         </div>
                       </div>
                     </div>
                 </div>
         </div>
       <?php endwhile; ?>
     <?php else: ?>
       <?php // no layouts found ?>
     <?php endif; ?>
   <?php endif; ?>
   <?php endwhile; ?>
   <?php else: ?>
   <?php // no layouts found ?>
   <?php endif; ?>


<?php get_footer(); ?>

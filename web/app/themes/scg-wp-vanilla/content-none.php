<?php
/**
 * The template for displaying a "No posts found" message.
 *
 * @subpackage scg
 */
?>

<article id="post-0" class="post no-results not-found">
    <header>
        <h2>Nothing Found</h2>
    </header>
    <div class="entry-content">
        <p>Apologies, but no results were found. Perhaps searching will help find a related post.</p>
    </div>
    <hr />
</article>

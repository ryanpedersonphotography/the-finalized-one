<?php
/*
 Template Name: Blog
*/
?>
<?php get_header(); ?>
<div class="reveal" id="mc_embed_signup" data-reveal>
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px; }
</style>
<form action="//scgpr.us1.list-manage.com/subscribe/post?u=c6e0f367d8bd088aa91fd8331&amp;id=01f09deb0c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
<img src="https://gallery.mailchimp.com/c6e0f367d8bd088aa91fd8331/images/519ebaff-ff34-4027-98b0-7044bca04640.jpg" />
<div class="mc-field-group">
	<label for="mce-FNAME">First Name </label>
	<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
</div>
<div class="mc-field-group">
	<label for="mce-LNAME">Last Name </label>
	<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
<div class="mc-field-group">
	<label for="mce-EMAIL">Email </label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c6e0f367d8bd088aa91fd8331_01f09deb0c" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
</div>
<!-- the loop -->
<?php while (have_posts()) : the_post(); ?>


<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START INTRO HEADLINE
//////////////////////////////
////////////////////////////// -->

<section class="hero" style="background: url('<?php
  $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

  if  (! empty($featured_image_url) ) :
    the_post_thumbnail_url('large');
  else:
     echo "https://dummyimage.com/2000x1200/c2c0c2/575757.png&text=needs+blog+hero";
  endif;
?>') no-repeat center center; background-size: cover; ">
</section>

<section class="section section-case-study-single">
    <div class="row">
      <div class="small-12 large-12 columns">
          <div class="row column text-center align-center"><hr class="dotted"></div>
            <h1 class="text-center"><?php the_title(); ?></h1>
            <div class="text-center">
              <?php
                $post_id = get_the_ID();
                $post_author = get_post_field( 'post_author', $post_id );
                $post_author_id = get_post_field( 'post_author', $post_id );
                $author_url = get_avatar_url( $post_author_id ); ?>
              <img src='<?php echo $author_url; ?>' class="author-circle" />
            </div>
            <div class="text-center">
                <h4 class"text-center">Written by <?php the_author(); ?> on <?php echo get_the_date(); ?></h4>
                <!-- <h4 class"text-center">Written by <?php the_author_posts_link(); ?> on <?php echo get_the_date(); ?></h4> -->
            </div>
            <div class="row column"><hr class="dotted"></div>
        </div>
    </div>
</section>

<section class="section-case-study section-case-study-single">
    <div class="row align-center">
      <div class="small-12 medium-8 columns align-middle">
          <p class="blog-text text-left" style="line-height:2.4em!important;">
            <?php
            the_content();
           ?>
          </p>
     </div>
    </div>


    <div class="row column"><hr class="dotted" style="margin-top:20px!important;"></div>
    <div class="text-center">
       <a  data-open="mc_embed_signup" target="_blank"><button class="large button btn-green">Sign Up For Email Updates</button></a>
    </div>
    <div class="row column"><hr class="dotted" style="margin-top:5px!important;"></div>
</section>

<?php get_footer();
 endwhile; ?>

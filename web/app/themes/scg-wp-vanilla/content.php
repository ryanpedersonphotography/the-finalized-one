<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @subpackage scg
 *
 */
?>

<article>
	<header>
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	</header>
	<div>
		<figure><a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('large'); } ?></a></figure> <?php the_excerpt(); ?>
	</div>
</article>
<div class="row column"><hr /></div>

<?php get_header(); ?>

<!-- Row for main content area -->
<section class="section">
    <div class="row column">
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article>
			<header>
				<h1 style="margin-top:.5em!important;"><?php the_title(); ?></h1>
        <img src="<?php the_post_thumbnail(); ?>"
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; // End the loop ?>
	</div>
</section>
	<?php get_sidebar(); ?>

<?php get_footer(); ?>

<?php
/*
 Template Name: Contact Us
*/
?>
<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
<section class="hero" style="background: url('<?php
  $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
  if  (! empty($featured_image_url) ) :
    the_post_thumbnail_url('full');
  else:
        // echo "<img src='" . get_template_directory_uri() . "/images/posts-plum.png' />";
     echo "https://dummyimage.com/2000x1200/c2c0c2/575757.png&text=contact+us";
  endif;
?>') no-repeat center center;  background-size: cover;">
<div class="row intro expanded">

</div>
</section>



<section class="section section-case-study-single">
    <div class="row">
      <div class="small-12 large-12 columns">
          <div class="row column text-center align-center"><hr class="dotted"></div>
            <h1 class="text-center">
                <?php the_field('case_study_headline'); ?>
                Contact Us
            </h1>
            <div class="text-center">
                <h4 class"text-center">
                    <?php the_field('case_study_sub-headline'); ?>
                    Fill out the form and we will be in touch with you shortly!
                </h4>
            </div>
            <div class="row column"><hr class="dotted"></div>
        </div>
    </div>
</section>

<section class="section section-posts text-center">
<div class="row columns medium-8">
        <div class="item contact-us-form material-card text-center">
      <?php the_content(); ?>
        </div>
</div>
</section>
<div class="row column"><hr /></div>
<?php endwhile; ?>
<?php get_footer(); ?>

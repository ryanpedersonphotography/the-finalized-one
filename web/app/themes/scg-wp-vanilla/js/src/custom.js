var displayNothing;


jQuery.noConflict();
jQuery(document).foundation();




(function($) {
  $(document).ready(function(){


});



   $(window).scroll(function() {
   var scroll = $(window).scrollTop();
   var templateUrl1 = object_name.templateUrl+'/images/SCG-notext.svg';
   var templateUrl2 = object_name.templateUrl+'/images/SCG.svg';

  if(scroll > 900) {
      $('#header, #header-fix').addClass('active');
      $('#header, #header-fix').addClass('transparent');
      $("#logo-scg").attr("src",templateUrl1);
  } else {
      $('#header, #header-fix').removeClass('active');
      $("#logo-scg").attr("src",templateUrl2);
  }

});

var owl = $("#owl-demo");

       owl.owlCarousel( {
lazyLoad: true,
           rewindNav : false,
   		      pagination : true,
           items : 4,
               afterAction: function(){
         if ( this.itemsAmount > this.visibleItems.length ) {
           $('.next').show();
$('.prev').show();

           $('.next').removeClass('disabled');
           $('.prev').removeClass('disabled');
           if ( this.currentItem === 0 ) {
             $('.prev').addClass('disabled');
}

if ( this.currentItem == this.maximumItem ) {
$('.next').addClass('disabled');
}

         }

else {
$('.next').hide();
           $('.prev').hide();
}
       }
       });

       //Custom Navigation Events
       $(".next").click(function() {
owl.trigger('owl.next');
});
       $(".prev").click(function() {
owl.trigger('owl.prev');
});

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

$( ".onMobileShow" ).show();
$( ".onMobileHide" ).hide();
$(".reveal-mobile").show();
$(".reveal-desktop").hide();


}

else {

$( ".onMobileShow" ).hide();
$( ".onMobileHide" ).show();
$(".reveal-mobile").hide();
$(".reveal-desktop").show();


}


function updateSize(){
        var minHeight=parseInt($('.owl-item').eq(0).css('height'));
        $('.owl-item').each(function () {
            var thisHeight = parseInt($(this).css('height'));
            minHeight=(minHeight<=thisHeight?minHeight:thisHeight);
        });
        $('.owl-wrapper-outer').css('height',minHeight+'px');

        $('.owl-carousel .owl-item img').each(function(){
            var thisHeight = parseInt($(this).css('height'));
            if(thisHeight>minHeight){
                $(this).css('margin-top',-1*(thisHeight-minHeight)+'px');
            }
        });

    }


$("#owl-demo").owlCarousel({
        afterUpdate: function () {
            updateSize();
        },
        afterInit:function(){
            updateSize();
        }
    });

$(".integrated-marketing, .unmatched-results, .item, .video-side-right, .video-side-left").hover(function(e) {
           $(this).find("video")[0].play();
},function(e) {
$(this).find("video")[0].pause();
});

setTimeout(function() {
$("video").each(function() {
               console.log(this);
               this.play();
               this.pause();
});
       },200);

})(jQuery);

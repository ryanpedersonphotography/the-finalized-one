<?php
/*
 Template Name: Landing Page - With Header/Footer
*/
?>
<?php get_header(); ?>
<!-- Row for main content area -->
<section class="">
    <div class="">
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; // End the loop ?>
	</div>
</section>
<?php get_footer(); ?>

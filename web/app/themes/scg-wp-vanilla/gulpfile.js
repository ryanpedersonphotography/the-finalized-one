var
	gulp 		    = require('gulp'),
	browserSync	= require('browser-sync').create(),
 	$    		    = require('gulp-load-plugins')(),
	notify = require('gulp-notify'),
  growl = require('gulp-notify-growl'),
	plumber = require('gulp-plumber'),
	gutil = require('gulp-util'),
	beep = require('beepbeep'),
	jshint = require('jshint')
;

const notifier = require('node-notifier');
const path = require('path');





var sassPaths = [
	'bower_components/foundation-sites/scss',
	'bower_components/motion-ui/src'
];

var owlCSS = [
	// Owl Carousel
	// 'bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
	'bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
	'bower_components/owl.carousel/dist/assets/owl.theme.default.css'
];

var owlJS = [
	// Owl Carousel
	'bower_components/owl.carousel/dist/owl.carousel.min.js'
];

var jsPaths = [
	'bower_components/foundation-sites/dist/foundation.js',
	// 'bower_components/owl.carousel/dist/owl.carousel.js',
	'bower_components/what-input/what-input.js',
	'node_modules/scrolltofixed/jquery-scrolltofixed.js',
	'js/*/*.js'

];

// Browser Sync
gulp.task('bs', function() {
	browserSync.init({
		proxy: 'scg-ryan.dev'
	})
});

// Copy Owl CSS
gulp.task('copy-owl-css', function() {
	gulp.src(owlCSS)
	.pipe(gulp.dest('css'));
});
// Copy Owl JS
gulp.task('copy-owl-js', function() {
	gulp.src(owlJS)
	.pipe(gulp.dest('js'));
});

// Sass
gulp.task('sass', function() {
	return gulp.src('scss/app.scss')
	.pipe($.sass({
		includePaths: sassPaths
	})
	.on('error', $.sass.logError))
	.pipe($.autoprefixer({
		browsers: ['last 2 versions', 'ie >= 9']
	}))
	.pipe(gulp.dest('css'))
	.pipe(browserSync.stream());

});


var gulp_src = gulp.src;
gulp.src = function() {
  return gulp_src.apply(gulp, arguments)
    .pipe(plumber(function(error) {


notifier.notify({
  title: error.plugin,
  message: error.message,
  sound: true, // Only Notification Center or Windows Toasters
  wait: true // Wait with callback, until user action is taken against notification
}, function (err, response) {
  // Response is response from notification
});



      // Output an error message
			beep()
      gutil.log(gutil.colors.blue('Error (' + error.plugin + '): ' + error.message));
      // emit the end event, to properly end the task
      this.emit('end');
    })
  );
};





// Javascript
gulp.task('js', function() {
	return gulp.src(jsPaths)
	.pipe($.jshint())
	.pipe($.concat('app.js'))
	.pipe($.uglify())
	.pipe(gulp.dest('js'))
	.pipe(browserSync.stream());
});


// Default task for watching
gulp.task('default', ['sass', 'js', 'bs','copy-owl-css','copy-owl-js'], function() {
	gulp.watch(['scss/**/*.scss'], ['sass']);
	gulp.watch(['bower_components/owl.carousel/dist/assets/*.css'], ['copy-owl-css'], ['copy-owl-js']);
	gulp.watch(['js/src/**/*.js'], ['js']);
	gulp.watch(['**/*.php', '**/*.html']).on('change', browserSync.reload);
});

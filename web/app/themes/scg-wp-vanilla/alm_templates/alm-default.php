            <div class="material-faster-card" style="margin-bottom:2em!important;">
              <a href="<?php the_permalink(); ?>" >
                   <div class="material-faster-card-image" style="background-image: url(<?php the_post_thumbnail_url('full'); ?>);">
                        <div class="material-faster-card-hide">Learn More!</div>
                   </div>
                <div class="material-faster-card-content"><?php the_title(); ?></div>
              </a>
            </div>

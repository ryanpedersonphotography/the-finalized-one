<?php
    get_header(); ?>
<section class="section-videos">
    <div class="video-side-left"  data-interchange="[<?php echo get_template_directory_uri(); ?>/inc/left-side-mobile.html, small], [<?php echo get_template_directory_uri(); ?>/inc/left-side-desktop.html, medium]">
    </div>
    <div class="video-side-right"  data-interchange="[<?php
        echo get_template_directory_uri(); ?>/inc/right-side-mobile.html, small], [<?php
        echo get_template_directory_uri(); ?>/inc/right-side-desktop.html, medium]">
    </div>
</section>
<div class="row column">
    <hr />
</div>
<section class="section text-center">
	<h2 class="animated fadeInRight">Our Work</h2>
	  	<div class="row material-faster-card-container">
			<!-- card group container -->
			<div class="material-faster-card">
				<a href="https://www.scgpr.com/our-work/cehd-case-study/">
					<div class="material-faster-card-image" style="background-image: url(https://www.scgpr.com/wp-content/uploads/2016/10/ourwork-cehd.png);">
						<div class="material-faster-card-hide">Learn More!</div>
					</div>
					<div class="material-faster-card-content">Going Global with University of Minnesota CEHD</div>
				</a>
			</div>
			<div class="material-faster-card">
				<a href="https://www.scgpr.com/our-work/rocket-league-case-study/">
					<div class="material-faster-card-image" style="background-image: url(https://www.scgpr.com/wp-content/uploads/2016/10/ourwork-rocketleague.png);">
						<div class="material-faster-card-hide">Learn More!</div>
					</div>
					<div class="material-faster-card-content">Revitalizing Rocket League’s Website</div>
				</a>
			</div>
			<div class="material-faster-card">
				<a href="http://www.scgpr.com/our-work/ratelinx-case-study/">
					<div class="material-faster-card-image" style="background-image: url(https://www.scgpr.com/wp-content/uploads/2016/10/ourwork-ratelinx.png);">
				   		<div class="material-faster-card-hide">Learn More!</div>
				    </div>
				    <div class="material-faster-card-content">Creating a Brand Identity for RateLinx</div>
				 </a>
			</div>
		</div>
</section>

      <!-- <div class="row material-faster-card-container">
                    <?php
                        $args = array(
                            'post_type' => 'casestudy',
                            'posts_per_page' => '3',
                        );
                        $the_query = new WP_Query($args);

                        while ($the_query->have_posts()):
                            $the_query->the_post(); ?>
                    <div class="material-faster-card">
                        <a href="<?php
                            the_permalink(); ?>">
                            <div class="material-faster-card-image" style="background-image: url(<?php
                                the_post_thumbnail_url('full'); ?>);">
                                <div class="material-faster-card-hide">Learn More!
                                </div>
                            </div>
                            <div class="material-faster-card-content">
                                <?php
                                    the_title(); ?>
                            </div>
                        </a>
                    </div>
                    <?php
                        endwhile;
                        wp_reset_postdata(); ?>
                </div> -->


<div class="reveal" id="contactus" data-reveal style="width: 75%!important;">
    <div class="row" style="margin-top:2em!important;">
        <div class="column small-10" >
            <h1 class="text-center  ">Contact Us!</h1>
            <div class="row expanded">
                <div class="column small-12">
                    <div class="text-center">
                        <?php
                            gravity_form(1, false, false, false, '', false); ?>
                    </div>
                </div>
            </div>
        </div>
        <button class="close-button" data-close aria-label="Close modal" type="button" style="margin-top:1.5em!important;">
        <span aria-hidden="true">&times;
        </span>
        </button>
    </div>
</div>
<div class="row column">
    <hr />
</div>
<section class="section section-integration-model">
    <div class="row align-center">
        <div class="columns small-12 text-center">
            <h2 class="animated fadeInRight">Our Capabilities</h2>
        </div>
            <!-- <div class="columns small-12 text-center"> -->
                  <div class="spinner">
                   <div class="inner-spin"></div>
                   <span class="spinner-content">
                     <h5>Public Relations</h5>
                     <p class="white"><br />Our PR, communications & media relations experts place stories across traditional, digital & broadcast media. Our internal communications capabilities will engage your employees.</p>
                     <img src="<?php echo get_template_directory_uri(); ?>/images/circle-green.png" alt="">
                   </span>
                 </div>
                     <div class="spinner">
                   <div class="inner-spin"></div>
                   <span class="spinner-content">
                     <h5>Branding</h5>
                     <p class="white"><br />SCG’s BrandVision℠ process creates a unique message platform that's the basis of our marketing and communications efforts, crafting a brand identity that’s built to last.</p>
                     <img src="<?php echo get_template_directory_uri(); ?>/images/circle-lime.png" alt="">
                   </span>
                 </div>
                     <div class="spinner">
                   <div class="inner-spin"></div>
                   <span class="spinner-content">
                     <h5>Research</h5>
                     <p class="white"><br />Through our IPQ Research™ process, we identify your brand’s strengths, opportunities and competitive advantages to create a plan perfectly tailored to grow your business.</p>
                     <img src="<?php echo get_template_directory_uri(); ?>/images/circle-gray.png" alt="">
                   </span>
                 </div>
                         <div class="spinner">
                   <div class="inner-spin"></div>
                   <span class="spinner-content">
                     <h5>Advertising</h5>
                     <p class="white"><br />We're experts in advertising & media buying in all channels – online, radio, print, billboard & television. We coordinate your campaign's content to achieve maximum impact.</p>
                     <img src="<?php echo get_template_directory_uri(); ?>/images/circle-darkpurple.png" alt="">
                   </span>
                 </div>
                         <div class="spinner">
                   <div class="inner-spin"></div>
                   <span class="spinner-content">
                     <h5>Digital</h5>
                     <p class="white"><br />We’ll create a multichannel digital marketing campaign that resonates, then use our expertise in content creation, SEO and display advertising to target your customers.</p>
                     <img src="<?php echo get_template_directory_uri(); ?>/images/circle-lavendar.png" alt="">
                   </span>
                 </div>
                         <div class="spinner">
                   <div class="inner-spin"></div>
                   <span class="spinner-content">
                     <h5>Sales Support</h5>
                     <p class="white"><br />SCG’s in-house design team helps you create a great first impression with brochures, sales sheets, signs and trade show graphics materials to make your brand stand out.</p>
                     <img src="<?php echo get_template_directory_uri(); ?>/images/circle-yellow.png" alt="">
                   </span>
                 </div>
                         <div class="spinner">
                   <div class="inner-spin"></div>
                   <span class="spinner-content">
                     <h5>Web Design</h5>
                     <p class="white"><br />Our web designers will revitalize your web presence with a fresh, mobile-optimized design. As an integrated agency, we can build a site that’s perfectly aligned with your marketing.</p>
                     <img src="<?php echo get_template_directory_uri(); ?>/images/circle-purple.png" alt="">
                   </span>
                 </div>
                         <div class="spinner">
                   <div class="inner-spin"></div>
                   <span class="spinner-content">
                     <h5>Creative</h5>
                     <p class="white"><br />Our experts have years of experience in multimedia & content writing. Whether it’s blogging, case studies, video or social media, we know how to captivate your customers.</p>
                     <img src="<?php echo get_template_directory_uri(); ?>/images/circle-yellow.png" alt="">
                   </span>
                 </div>
            <!-- </div> -->
        <!-- </div> -->
    </div>
</section>
<div class="row column">
    <hr />
</div>
<section class="section section-integration-model">
    <div class="row align-center">
        <div class="column small-12 text-center">
            <h2 class="animated fadeInRight">
                <?php
                    the_field('integration_model_header_text', 'option'); ?>
            </h2>
        </div>
        <div class="column small-10 text-center model-text">
            <p>
                <?php
                    the_field('integration_model_description_text', 'option'); ?>
            </p>
        </div>
        <div class="column small-12">
            <img src="<?php
                echo get_template_directory_uri(); ?>/images/model-horizontal.png" alt="" class="show-for-medium animated fadeIn">
            <img src="<?php
                echo get_template_directory_uri(); ?>/images/model-vertical.png" alt="" class="show-for-small-only animated fadeIn">
        </div>
    </div>
</section>
<div class="row">
    <hr />
</div>
<div class="row column">
    <hr />
</div>



<section class="section section-clients text-center ">
    <h2 class="animated fadeInLeft">Clients
    </h2>
    <div class="row small-8 medium-up-10 text-center align-center" style="margin-bottom:2em;">
        <?php
            if (have_rows('logos', 'option')):
            while (have_rows('logos', 'option')):
                the_row();

            if (get_sub_field('logo_images'))
                {
        ?>
        <div class="columns small-4 medium-3">
            <img src="<?php
                the_sub_field('logo_images'); ?>">
        </div>
        <?php
            }
            endwhile;
            else:
            endif;
        ?>
    </div>
</section>
<div class="row column">
    <hr />
</div>

<section class="section text-center">
    <h2 class="animated fadeInRight">Recent Posts
    </h2>
    <div class="row material-faster-card-container">
        <?php
            $args = array(
                'posts_per_page' => '3',
            );
            $the_query = new WP_Query($args);

            while ($the_query->have_posts()):
                $the_query->the_post(); ?>
        <div class="material-faster-card">
            <a href="<?php
                the_permalink(); ?>" >
                <div class="material-faster-card-image" style="background-image: url(<?php
                    the_post_thumbnail_url('full'); ?>);">
                    <div class="material-faster-card-hide">Learn More!
                    </div>
                </div>
                <div class="material-faster-card-content">
                    <?php
                        the_title(); ?>
                </div>
            </a>
        </div>
        <?php
            endwhile;
            wp_reset_postdata(); ?>
    </div>
    <div class="column row text-center align-center" id="owl-nav">
        <a href="/our-blog">
            <flat-button-2 class="read-more next">View All Posts
            </flat-button-2>
        </a>
    </div>
</section>
<div class="row column">
    <hr />
</div>

<section class="section text-center">
    <h2 class="animated fadeInRight">Testimonials
    </h2>
    <div class="row material-testimonial-card-container">

                <div class="material-testimonial-card">
        						<div class="material-testimonial-card-content">
                                    “As a busy business owner I didn’t have time to focus on our branding and communication messages. SCG has made it very easy, and it’s gotten us some amazing business opportunities. If you are looking for fast, profitable growth, I recommend working with SCG. They are a great marketing partner…”
        						</div>
        						<div class="material-testimonial-card-title">
        								Shannon Vaillancourt<br />
                                        RateLinx
        						</div>

        		</div>
        		<div class="material-testimonial-card">
        						<div class="material-testimonial-card-content">
        							 “Feedback has been overwhelmingly and unanimously positive to the new website and the brand updates. Thanks again for the amazing work you’ve done. The extended IWCO Direct Sales family appreciates it, too.”
        						</div>
        						<div class="material-testimonial-card-title">
        								Debora Haskel <br />
                                        IWCO Direct
        						</div>

        		</div>
        		<div class="material-testimonial-card">
        						<div class="material-testimonial-card-content">
        							 “To tell our company’s story to potential customers and employees in a compelling way, we worked with SCG to create a suite of videos. From planning to creative execution, they were easy to work with. The on-site shoot included a drone and captured dramatic shots of our facility. I love the way the videos turned out.”
        						</div>
        						<div class="material-testimonial-card-title">
        								Cody Cuhel <br />
                                        Millerbernd Mfg
        						</div>

        		</div>
    </div>
</section>
<div class="row column">
    <hr />
</div>
<section class="section section-page-header section-bottom-banner" style="background: url(<?php
    echo get_template_directory_uri(); ?>/images/banner-background-citrus.svg);
    background-size: cover;">
    <div class="row collapse expanded text-center">
        <div class="medium-12 columns">
            <div class="page-header-billboard">
                <div class="inner ">
                    <h2 class="animated fadeInRight">
                        <?php
                            the_field('bottom_banner_blurb', 'option'); ?>
                    </h2>
                    <a data-open="contactus">
                    <button class="large button btn-green">
                    <?php
                        the_field('bottom_banner_button_text', 'option'); ?>
                    </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="row column">
    <hr />
</div>
<?php
    get_footer(); ?>

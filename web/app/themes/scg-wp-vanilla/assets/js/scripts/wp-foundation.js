/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {

    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();

	 // Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass( 'end' );

	// Adds Flex Video to YouTube and Vimeo Embeds
  jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
    if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
      jQuery(this).wrap("<div class='widescreen flex-video'/>");
    } else {
      jQuery(this).wrap("<div class='flex-video'/>");
    }
  });

});



(function($) {
    $.fn.ready(function() {

        $(".integrated-marketing, .unmatched-results").hover(function(e) {
            $(this).find("video")[0].play();
        }, function(e) {
            $(this).find("video")[0].pause();
        })
        setTimeout(function() {
            $("video").each(function() {
                console.log(this);
                this.play();
                this.pause();
            })
        }, 200);
    });
})(jQuery);


$(window).scroll(function() {
	var scroll = $(window).scrollTop();
	if(scroll > 0) {
		$('#header, #header-fix').addClass('active');
		$('#header, #header-fix').addClass('transparent');
		$("#logo-scg").attr("src","<?php echo get_template_directory_uri(); ?>/images/SCG-notext.svg");
	} else {
		$('#header, #header-fix').removeClass('active');
		$("#logo-scg").attr("src","<?php echo get_template_directory_uri(); ?>/images/SCG.svg");

	}
});

    (function($) {
        $.fn.ready(function() {

            $("#imitation3, #interstellar3").hover(function(e) {
                $(this).find("video")[0].play();
            }, function(e) {
                $(this).find("video")[0].pause();
            })
            setTimeout(function() {
                $("video").each(function() {
                    console.log(this);
                    this.play();
                    this.pause();
                })
            }, 200);
        });
    })(jQuery);

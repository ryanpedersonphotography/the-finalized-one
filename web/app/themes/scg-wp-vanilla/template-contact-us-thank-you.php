<?php
/*
 Template Name: Contact Us Thank You
*/
?>
<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
<section class="hero">
<div class="row intro expanded">
</div>
</section>



<section class="section section-case-study-single">
    <div class="row">
      <div class="small-12 large-12 columns">
          <div class="row column text-center align-center"><hr class="dotted"></div>
            <h1 class="text-center">
                Thank you!
            </h1>
            <div class="text-center">
                <h4 class"text-center">
                    Thanks for contacting us! We will be in touch with you shortly.
                </h4>
            </div>
            <div class="row column"><hr class="dotted"></div>
        </div>
    </div>
</section>

<!-- <section class="section section-posts text-center">
<div class="row columns medium-8">
        <div class="item contact-us-form material-card text-center">
      <?php the_content(); ?>
        </div>
</div>
</section> -->
<!-- <div class="row column"><hr /></div> -->
<?php endwhile; ?>
<?php get_footer(); ?>

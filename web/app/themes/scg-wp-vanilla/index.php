<?php get_header(); ?>
	<section class="section">
		<div class="row column">
<?php
if (have_posts()):
    while (have_posts()):
        the_post();
        get_template_part('content', get_post_format());
    endwhile;
else:
    get_template_part('content', 'none');
endif; // end have_posts() check
?>
		</div>
	</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>

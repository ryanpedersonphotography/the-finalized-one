<?php
/*
 Template Name: Latest Blogs
*/
?>
<?php get_header(); ?>

<div class="reveal" id="mc_embed_signup" data-reveal>
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px; }
</style>
<form action="//scgpr.us1.list-manage.com/subscribe/post?u=c6e0f367d8bd088aa91fd8331&amp;id=01f09deb0c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
<img src="https://gallery.mailchimp.com/c6e0f367d8bd088aa91fd8331/images/519ebaff-ff34-4027-98b0-7044bca04640.jpg" />
<div class="mc-field-group">
	<label for="mce-FNAME">First Name </label>
	<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
</div>
<div class="mc-field-group">
	<label for="mce-LNAME">Last Name </label>
	<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
<div class="mc-field-group">
	<label for="mce-EMAIL">Email </label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c6e0f367d8bd088aa91fd8331_01f09deb0c" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
</div>

<?php while (have_posts()) : the_post(); ?>
<section class="hero" style="background: url('<?php
  $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
  if  (! empty($featured_image_url) ) :
    the_post_thumbnail_url('full');
  else:
  endif;
?>') no-repeat center center;  background-size: cover;">
<div class="row intro expanded">

</div>
</section>
<section class="section section-case-study-single">
    <div class="row">
      <div class="small-12 large-12 columns">
          <div class="row column text-center align-center"><hr class="dotted"></div>
            <h1 class="text-center">41 Stories</h1>
            <h5 class="text-center"><a  data-open="mc_embed_signup" target="_blank">From the top floor of the Campbell Mithun Tower, SCG’s team blogs about the latest marketing trends, technology and best practices.</a></h5>
            <div class="text-center">
            </div>
            <div class="row column"><hr class="dotted"></div>
        </div>
    </div>
</section>

<div class="row material-card-container" >

  <?php echo do_shortcode('[ajax_load_more theme_repeater="alm-default.php" post_type="post" posts_per_page="6" transition="fade"]'); ?>

</div> <!-- close row -->




<div class="row column"><hr /></div>
<?php endwhile; ?>
<?php get_footer(); ?>

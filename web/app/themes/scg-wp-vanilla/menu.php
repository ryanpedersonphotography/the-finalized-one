<div class="off-canvas position-right" id="offCanvas" data-off-canvas data-position="right">
<div style="background:white!important;">
   <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/sittingplum.svg" id="logo-scg" alt="" style="width:20%!important;margin-bottom:-30px!important;margin-left:25%!important;"></a>
</div>
<div style="background:white!important;padding-left:50px;">
    <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/SCG-notext.svg" id="logo-scg" alt="" style="width:75%!important;"></a>
</div>
   <?php
    wp_nav_menu( array(
        'theme_location' => 'scg-menu-mobile',
        'menu_class' => '',
        'container' => 'ul' ) );
    ?>
</div>

<div class="off-canvas-content" data-off-canvas-content>
    <div class="title-bar hide-for-medium" style="background: rgba(255,255,255,.4)!important;">
        <div class="title-bar-left">
            <span class="title-bar-title"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/SCG-notext.svg" id="logo-scg" alt="" style="width:50%!important;"></a></span>
        </div>
        <div class="title-bar-right">
            <button class="menu-icon dark" type="button" data-toggle="offCanvas"></button>
        </div>
    </div>

<?php  if (is_front_page()): ?>
 <?php   echo'<div id="header-fix" class="header-fix show-for-medium"></div>'; ?>
<?php endif; ?>
    <header id="header" class="header box-shadow show-for-medium">
        <div class="row text-center">
            <div class="medium-5 columns">
                <nav>
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'scg-menu-left',
                        'container' => 'ul',
                        'menu_class' => '' ) );
                    ?>
                </nav>
            </div>
            <div class="medium-2 columns">
                <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/SCG.svg" alt=""  id="logo-scg"></a>
            </div>
            <div class="medium-5 columns">
                <nav>
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'scg-menu-right',
                        'container' => 'ul',
                        'menu_class' => '' ) );
                    ?>
                </nav>
            </div>
        </div>
    </header>

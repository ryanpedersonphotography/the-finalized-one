<?php
/*
 Template Name: Landing Page - With Footer (No Header)
*/
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php wp_title( '|', true, 'right' ); ?>
    </title>
    <script src="https://use.typekit.net/xuw3czl.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.12.1/TweenMax.min.js"></script>
    <script>
        try {
            Typekit.load({
                async: true
            });
        } catch (e) {}
    </script>
    <?php wp_head(); ?>
</head>
<body>
<!-- Row for main content area -->
<section class="">
    <div class="">
	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; // End the loop ?>
	</div>
</section>
<?php get_footer(); ?>

<?php
/*
 Template Name: Single Case Study
*/
?>
<?php get_header(); ?>
<?php
      if (!get_field( 'intro_quote')):
        $width_intro = "55%";
      else:
        $width_intro = "35%";
      endif;

      if (!get_field( 'spark_quote')):
        $width_spark = "55%";
      else:
        $width_spark = "35%";
      endif;

      if (!get_field( 'simplify_quote')):
        $width_simplify = "55%";
      else:
        $width_simplify = "35%";
      endif;

      if (!get_field( 'soar_quote')):
        $width_soar = "55%";
      else:
        $width_soar = "35%";
      endif;
 ?>

 <style>
 .menu-icon::after {
     background:  #212121!important;
     box-shadow: 0 7px 0 #212121, 0 14px 0 #212121 !important;
 }
 </style>
<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START INTRO HEADLINE
//////////////////////////////
////////////////////////////// -->

<section class="hero" style="background: url('<?php the_field('hero_banner_image'); ?>') no-repeat center center;  background-size: cover;">
  <div class="row intro expanded">
    <!-- <div class="columns">
      <h1><?php the_title(); ?></h1>
      <p>Going Global with University of Minnesota CEHD</p>
    </div> -->
    <!-- <div class="small-centered medium-uncentered medium-6 large-5 columns">
      <div class="tech-img"></div>
    </div> -->
  </div>
</section>

<?php if (get_field( 'intro_text')): ?>

<section class="section section-case-study-single">
    <div class="row">
      <div class="small-12 large-12 columns">
          <div class="row column text-center align-center case-study-margin-top"><hr class="dotted"></div>
            <h1 class="text-center">
                <?php the_field('case_study_headline'); ?>
            </h1>
            <div class="text-center">
                <h4 class"text-center">
                    <?php the_field('case_study_sub-headline'); ?>
                </h4>
            </div>
            <div class="row column"><hr class="dotted"></div>
        </div>
    </div>
</section>

<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START INTRO CONTENT
//////////////////////////////
////////////////////////////// -->


<section class="section-case-study section-case-study-single">
    <div class="row">
      <div class="small-12 medium-6 columns align-middle">
          <div class="text-center">
              <strong>Introduction</strong>
          </div>
          <p class="blog-text text-left">
                <?php the_field('intro_text'); ?>
          </p>
      </div>

      <div class="small-12 medium-6 columns align-middle text-center">
          <div class="row small-12 medium-12 text-center align-center">
              <div class="column small-12 medium-12 text-center align-center">
                <blockquote class="cite">
                    <img src="<?php the_field('intro_image'); ?>" style="border-radius: 50%!important; width:<?php echo $width_intro; ?>;"  />
                    <p>
                        <?php the_field('intro_quote'); ?>
                    </p>
                </blockquote>
            </div>
          </div>
      </div>
    </div>
    <div class="row column case-study-margin-bottom"><hr class="dotted"></div>
</section>

<?php endif; ?>

<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START SPARK HEADLINE
//////////////////////////////
//////////////////////////////
-->


<div style="margin-top:0px;margin-bottom:0px;padding-top:1em;padding-bottom:1em;">
    <label style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/banner-background-orange.svg');
    height:auto;
    width:100vw;
    display:inline-block;
    background-size:cover;
    padding-top:1.5em;
    padding-bottom:1.5em;
    overflow:visible;box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;">
        <div style="text-align:center;width:90vw;"class="row column text-center">
<p style="text-shadow: 3px 3px 7px rgba(0,0,0, 0.62)!important;color:white!important;font-size:2.5em!important;">
SPARK
</p></div>
    </label>
</div>

<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START SPARK CONTENT
//////////////////////////////
//////////////////////////////
 -->

<section class="section-case-study section-case-study-single">
    <div class="row column case-study-margin-top"><hr class="dotted" ></div>
    <div class="row">

      <?php if ((!get_field( 'spark_quote')) || (!get_field( 'spark_image'))) : ?>

            <div class="small-12 columns align-middle">
                <p class="blog-text text-left"><?php the_field('spark_text'); ?></p>
            </div>
          </div>

        <?php
      else:
         ?>


        <div class="small-12 medium-6 columns align-middle text-center">
            <div class="row small-12 medium-12 text-center align-center">
                <div class="column small-12 medium-12">
                    <blockquote class="cite-left">
                      <img src="<?php the_field('spark_image'); ?>"  style="border-radius: 50%!important; width:<?php echo $width_spark; ?>;" />
                      <p>
                          <?php the_field('spark_quote'); ?>
                      </p>
                  </blockquote>
              </div>
            </div>
        </div>
        <div class="small-12 medium-6 columns align-middle">
            <p class="blog-text text-left"><?php the_field('spark_text'); ?></p>
        </div>
      </div>


<?php endif; ?>



    <div class="row column case-study-margin-bottom"><hr class="dotted"></div>
</section>


<!-- //////////////////////////////
//////////////////////////////
////////////////////////////// START SIMPLIFY SECTION
//////////////////////////////
////////////////////////////// -->

<div style="margin-top:0px;margin-bottom:0px;padding-top:1em;padding-bottom:1em;">
    <label style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/banner-background-purple.svg');
    height:auto;
    width:100vw;
    display:inline-block;
    background-size:cover;
    padding-top:1.5em;
    padding-bottom:1.5em;
    overflow:visible;box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;">
        <div style="text-align:center;width:90vw;"class="row column text-center">
<p style="text-shadow: 3px 3px 7px rgba(0,0,0, 0.62)!important;color:white!important;font-size:2.5em!important;">
SIMPLIFY
</p></div>
    </label>
</div>


<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START SIMPLIFY CONTENT
//////////////////////////////
//////////////////////////////
 -->

<section class="section-case-study section-case-study-single">
    <div class="row column case-study-margin-top"><hr class="dotted"></div>
    <div class="row">


 <?php if ((!get_field( 'simplify_quote')) || (!get_field( 'simplify_image'))) : ?>

      <div class="small-12 columns align-middle">
          <p class="blog-text text-left">
                <?php the_field('simplify_text'); ?>
          </p>
      </div>

<?php else:  ?>

      <div class="small-12 medium-6 columns align-middle">
          <p class="blog-text text-left">
                <?php the_field('simplify_text'); ?>
          </p>
      </div>


      <div class="small-12 medium-6 columns align-middle text-center">
          <div class="row small-12 text-center align-center">
              <div class="column small-12 medium-12">
                <blockquote class="cite">
                    <img src="<?php the_field('simplify_image'); ?>" style="border-radius: 50%!important;width:<?php echo $width_simplify; ?>;" />
                    <p><?php the_field('simplify_quote'); ?></p>
                </blockquote>
            </div>
          </div>
      </div>

    <?php endif; ?>

  </div>
  <div class="row column case-study-margin-bottom"><hr class="dotted"></div>
</section>


<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// SOAR HEADER
//////////////////////////////
//////////////////////////////
 -->

<div style="margin-top:0px;margin-bottom:0px;padding-top:1em;padding-bottom:1em;">
    <label style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/banner-background-citrus.svg');
    height:auto;
    width:100vw;
    display:inline-block;
    background-size:cover;
    padding-top:1.5em;
    padding-bottom:1.5em;
    overflow:visible;box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;">
        <div style="text-align:center;width:90vw;"class="row column text-center">
<p style="text-shadow: 3px 3px 7px rgba(0,0,0, 0.62)!important;color:white!important;font-size:2.5em!important;">
SOAR
</p></div>
    </label>
</div>

<section class="section section-case-study section-case-study-single" style="margin:0!important;padding-left:4.0em!important;padding-right:4em!important;">
</section>


<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START SOAR CONTENT
//////////////////////////////
//////////////////////////////
 -->

<section class="section-case-study section-case-study-single">
    <div class="row column case-study-margin-top"><hr class="dotted"></div>
    <div class="row">

      <?php if ((!get_field( 'soar_quote')) || (!get_field( 'soar_image'))) : ?>

            <div class="small-12 columns align-middle">
                <p class="blog-text text-left"><?php the_field('soar_text'); ?></p>
            </div>
          </div>

        <?php
      else:
         ?>


        <div class="small-12 medium-6 columns align-middle text-center">
            <div class="row small-12 medium-6 text-center align-center">
                <div class="column small-12 medium-12">
                    <blockquote class="cite-left">
                      <img src="<?php the_field('soar_image'); ?>" style="border-radius: 50%!important; width:<?php echo $width_soar; ?>;" />
                      <p>
                          <?php the_field('soar_quote'); ?>
                      </p>
                  </blockquote>
              </div>
            </div>
        </div>
        <div class="small-12 medium-6 columns align-middle">
            <p class="blog-text text-left"><?php the_field('soar_text'); ?></p>
        </div>
      </div>


<?php endif; ?>



    <div class="row column case-study-margin-bottom"><hr class="dotted"></div>
</section>



<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// RESULTS HEADER
//////////////////////////////
//////////////////////////////
 -->

<div style="margin-top:0px;margin-bottom:0px;padding-top:1em;padding-bottom:1em;">
    <label style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/banner-background-green.svg');
    height:auto;
    width:100vw;
    display:inline-block;
    background-size:cover;
    padding-top:1.5em;
    padding-bottom:1.5em;
    overflow:visible;box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;">
        <div style="text-align:center;width:90vw;"class="row column text-center">
<p style="text-shadow: 3px 3px 7px rgba(0,0,0, 0.62)!important;color:white!important;font-size:2.5em!important;">
THE RESULTS
</p></div>
    </label>
</div>

<section class="section section-case-study section-case-study-single" style="margin:0!important;padding-left:4.0em!important;padding-right:4em!important;">
    <div class="row column case-study-margin-top"><hr class="dotted"></div>
</section>

<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// RESULTS CONTENT
//////////////////////////////
//////////////////////////////
 -->

<div class="row small-up-10 medium-up-4 align-center">
  <?php if ( have_rows( 'results_info_blurbs' ) ): ?>
    <?php while ( have_rows( 'results_info_blurbs' ) ) : the_row(); ?>

           <div class="columns small-6 medium-3">
              <div class="circle-<?php the_sub_field( 'blurb_circle_color' ); ?>">
                <div class="circle__inner">
                  <div class="circle__wrapper">
                    <div class="circle__content">

<?php

$blurb_count = strlen(get_sub_field('results_blurb_large_text'));
if ($blurb_count < 4):
  $fontsize = "8vw";
  $lineheight = '5vw';
  elseif (($blurb_count > 6) && ($blurb_count < 12)):
  $fontsize = "4vw";
  $lineheight = '3vw';
else:
  $fontsize = "4vw";
  $lineheight = '17vw';
endif;

?>
<h2 style="font-size:<?php echo $fontsize; ?>!important;line-height:<?php echo $lineheight; ?>;color:white;">
  <span style="text-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);">
    <?php the_sub_field( 'results_blurb_large_text' ); ?>
  </span>
</h2>
                      <h5 style="color:white;">
                        <span style="text-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);">
                          <?php the_sub_field( 'results_blurb_small_text' ); ?>
                        </span>
                      </h5>

                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
     <?php endwhile; ?>
   <?php else: ?>
   <?php endif; ?>
</div>






  <div class="row column case-study-margin-bottom"><hr class="dotted"></div>
  <?php
  // endif;
   ?>

<?php get_footer(); ?>

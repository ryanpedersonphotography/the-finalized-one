<?php
/*
 Template Name: Our Team
*/
?>
<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>


<section class="hero" style="background: url('<?php
  $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
  if  (! empty($featured_image_url) ) :
    the_post_thumbnail_url('full');
  else:
  endif;
?>') no-repeat center center;  background-size: cover;">
<div class="row intro expanded">

</div>
</section>
<?php endwhile; ?>

<section class="section section-case-study-single">
    <div class="row">
      <div class="small-12 large-12 columns">
          <div class="row column text-center align-center"><hr class="dotted"></div>
            <h1 class="text-center">
                Our Team
              </h1>
            <div class="text-center">
                <h4 class"text-center">
                    <!-- what makes us strong... -->
                </h4>
            </div>
            <div class="row column"><hr class="dotted"></div>
        </div>
    </div>
</section>
<section class="section section-our-team text-center">
<div class="row medium-up-3" >
<?php $loop = new WP_Query( array(
        'post_type' => 'employee',
        'posts_per_page' => 100,
        'orderby' => 'menu_order',
        'order' => 'ASC'
         )
            );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>

        <!-- <div class="reveal" id=<?php the_title(); ?> data-reveal style="<?php if (wp_is_mobile()): echo' width:100!important; '; else: echo' width:80%!important; '; endif; ?> padding-top:4em;"> -->

        <div class="reveal reveal-mobile reveal-desktop" id=<?php the_title(); ?> data-reveal style="padding-top:4em;">
            <div class="row">
                <div class="column small-12">
                  <h1 class="text-center"><?php the_title(); ?></h1>
                  <h5 class="text-center"><?php the_field('job_title'); ?></h5>
                  <p class="lead"><?php the_field('employee_blurb'); ?></p>
                </div>
                <!-- <div class="column small-12 medium-6"><img src="<?php the_field('employee_photo_1');?>"/></div>
                <div class="column small-12 medium-6"><img src="<?php the_field('employee_photo_2');?>"/></div> -->
            </div>

            <button class="close-button" data-close aria-label="Close modal" type="button" style="margin-top:2em!important;">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>


    <div class="columns small-12">
        <a data-open="<?php the_title(); ?>">
        <div class="item">
          <img src="<?php the_field( 'mobile_photo' ); ?>" height="300px" class="onMobileShow"/>
          <?php

          $video_file = get_field('employee_video');
          if ($video_file)  {  ?>
            <video loop="true" class="onMobileHide" height="300px;">
                <source src="<?php the_field('employee_video'); ?>" type="video/mp4" class="onMobileHide">
            </video>
           <?php } else { ?>
                            <img src="<?php the_field( 'mobile_photo' ); ?>" class="onMobileHide" style="height:307px!important;width:auto!important;"/>
            <?php }  ?>

            <h5><?php the_title(); ?></h5>
        </div>
        </a>
    </div>
    <?php endwhile; wp_reset_query(); ?>
  </div>
</section>
<div class="row column"><hr /></div>

<?php get_footer(); ?>

<?php
/*
 Template Name: About Us 
*/
?>
<?php get_header(); ?>
<style>
.menu-icon::after {
    background:  #212121!important;
    box-shadow: 0 7px 0 #212121, 0 14px 0 #212121 !important;
}
</style>
<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START INTRO HEADLINE
////////////////////////////// http://scg-ryan.dev/app/uploads/E3A9387-1.jpg
////////////////////////////// -->

<section class="hero" style="background: url('<?php echo get_template_directory_uri(); ?>/images/E3A9387-1.jpg') no-repeat center center;  background-size: cover;">
  <div class="row intro expanded">
  </div>
</section>

<section class="section section-case-study-single">
    <div class="row">
      <div class="small-12 large-12 columns">
          <div class="row column text-center align-center"><hr class="dotted"></div>
            <h1 class="text-center">
                <?php the_field('about_us_headline_text', 'option'); ?>
            </h1>
            <div class="text-center">
                <h4 class"text-center">
                    <?php the_field('about_us_sub_headline_text', 'option'); ?>
                </h4>
            </div>
            <div class="row column"><hr class="dotted"></div>
        </div>
    </div>
</section>

<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START INTRO CONTENT
//////////////////////////////
////////////////////////////// -->



<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START SPARK HEADLINE
//////////////////////////////
//////////////////////////////
-->

<section class="section section-page-header-case-study"  style="height:5rem!important;">
    <div class="row collapse expanded text-center">
        <div class="medium-12 columns">
            <div class="page-header-billboard">
                <div class="inner">
                  <h2 style="font-size:40px!important; margin-top:7px!important;">SPARK</h2>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/images/banner-background-orange.svg">
                <!-- <div class="overlay" style="opacity:.5!important;"></div> -->
            </div>
        </div>
    </div>
</section>

<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START SPARK CONTENT
//////////////////////////////
//////////////////////////////
 -->

<section class="section-case-study section-case-study-single">
    <div class="row column"><hr class="dotted" style="margin-top:20px!important;"></div>
    <div class="row">
        <div class="small-12 medium-6 columns align-middle text-center">
            <div class="row small-12 medium-6 text-center align-center">
                <div class="column small-12 medium-12">
                    <blockquote class="cite-left">
                      <?php if ( get_field( 'about_us_spark_image','option') ) { ?>
                      	<img src="<?php the_field( 'about_us_spark_image', 'option' ); ?>" style="border-radius: 50%!important; width:60%;"  />
                      <?php } ?>
                      <!-- <p>
                        <?php the_field( 'about_us_spark_quote', 'option' ); ?>
                      </p> -->
                  </blockquote>
              </div>
            </div>
        </div>
      <div class="small-12 medium-6 columns align-middle">
          <p class="blog-text text-left">
                <?php the_field('about_us_spark_text', 'option'); ?>
          </p>
      </div>
    </div>
    <div class="row column"><hr class="dotted"></div>
</section>


<!-- //////////////////////////////
//////////////////////////////
////////////////////////////// BEGIN SIMPLIFY HEADER
//////////////////////////////
////////////////////////////// -->


<section class="section section-page-header-case-study" style="height:5rem!important;">
    <div class="row collapse expanded text-center">
        <div class="medium-12 columns" >
            <div class="page-header-billboard"  >
                <div class="inner">
                  <h2 style="font-size:40px!important; margin-top:7px!important;">SIMPLIFY</h2>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/images/banner-background-purple.svg">
                <!-- <div class="overlay" style="opacity:.5!important;"></div> -->
            </div>
        </div>
    </div>
</section>

<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// BEGIN SIMPLIFY CONTENT
//////////////////////////////
//////////////////////////////
 -->

<section class="section-case-study section-case-study-single">
    <div class="row column"><hr class="dotted"></div>
    <div class="row">
      <div class="small-12 medium-6 columns align-middle">
          <p class="blog-text text-left">
                <?php the_field('about_us_simplify_text','option'); ?>
          </p>
      </div>
      <div class="small-12 medium-6 columns align-middle text-center">
          <div class="row small-12 medium-6 text-center align-center">
              <div class="column small-12 medium-12">
                <blockquote class="cite">
                    <img src="<?php the_field('about_us_simplify_image','option'); ?>" style="border-radius: 50%!important; width:60%;" />
                    <!-- <p><?php the_field('about_us_simplify_quote','option'); ?></p> -->
                </blockquote>
            </div>
          </div>
      </div>
    </div>
    <div class="row column"><hr class="dotted"></div>
</section>


<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// SOAR HEADER
//////////////////////////////
//////////////////////////////
 -->

<section class="section section-page-header-case-study" style="height:5rem!important;">
    <div class="row collapse expanded text-center">
        <div class="medium-12 columns">
            <div class="page-header-billboard">
                <div class="inner">
                    <h2 style="font-size:40px!important; margin-top:7px!important;">SOAR</h2>
                </div>
                <img src="<?php echo get_template_directory_uri(); ?>/images/banner-background-citrus.svg">
                <!-- <div class="overlay" style="opacity:.5!important;"></div> -->
            </div>
        </div>
    </div>
</section>
<section class="section section-case-study section-case-study-single">
    <div class="row column"><hr class="dotted"></div>
</section>

<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// SOAR CONTENT
//////////////////////////////
//////////////////////////////
 -->

 <section class="section-case-study section-case-study-single">
     <div class="row">
         <div class="small-12 medium-6 columns align-middle text-center">
             <div class="row small-12 medium-6 text-center align-center">
                 <div class="column small-12 medium-12">
                     <blockquote class="cite-left">
                       <?php if ( get_field( 'about_us_soar_image','option') ) { ?>
                       	<img src="<?php the_field( 'about_us_soar_image', 'option' ); ?>" style="border-radius: 50%!important; width:60%;"  />
                       <?php } ?>
                       <!-- <p>
                         <?php the_field( 'about_us_soar_quote', 'option' ); ?>
                       </p> -->
                   </blockquote>
               </div>
             </div>
         </div>
       <div class="small-12 medium-6 columns align-middle">
           <p class="blog-text text-left">
                 <?php the_field('about_us_soar_text', 'option'); ?>
           </p>
       </div>
     </div>
     <div class="row column"><hr class="dotted"></div>
 </section>


 <!--
 //////////////////////////////
 //////////////////////////////
 ////////////////////////////// APPROACH HEADER
 //////////////////////////////
 //////////////////////////////
  -->
<div style="margin-top:0px;margin-bottom:0px;padding-top:1em;padding-bottom:1em;">
    <label style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/banner-background-green.svg');
    height:auto;
    width:100vw;
    display:inline-block;
    background-size:cover;
    padding-top:1.5em;
    padding-bottom:1.5em;
    overflow:visible;box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;">
        <div style="text-align:center;width:90vw;"class="row column text-center">
<p style="text-shadow: 3px 3px 7px rgba(0,0,0, 0.62)!important;color:white!important;font-size:2.5em!important;">
OUR CLIENTS SOAR
</p></div>
    </label>
</div>

 <section class="section section-case-study section-case-study-single">
     <div class="row column"><hr class="dotted"></div>
 </section>

 <!--
 //////////////////////////////
 //////////////////////////////
 ////////////////////////////// APPROACH CONTENT
 //////////////////////////////
 //////////////////////////////
  -->

  <section class="section-case-study section-case-study-single">
      <!-- <div class="row column"><hr class="dotted"></div>
      <div class="row column"><hr class="dotted"></div> -->

      <div class="row">
        <div class="small-12 medium-6 columns align-middle">
            <p class="blog-text text-left">
                  <?php the_field('about_us_approach_text','option'); ?>
            </p>
        </div>
        <div class="small-12 medium-6 columns align-middle text-center">
            <div class="row small-12 medium-6 text-center align-center">
                <div class="column small-12 medium-12">
                  <blockquote class="cite">
                      <img src="<?php the_field('about_us_approach_image','option'); ?>" style="border-radius: 50%!important; width:60%;" />
                      <!-- <p><?php the_field('about_us_approach_quote','option'); ?></p> -->
                  </blockquote>
              </div>
            </div>
        </div>
      </div>
      <div class="row column"><hr class="dotted"></div>
  </section>

  <!--
  //////////////////////////////
  //////////////////////////////
  ////////////////////////////// CAPABILITIES HEADER
  //////////////////////////////
  //////////////////////////////
   -->

  <!-- <section class="section section-page-header-case-study capability">
      <div class="row collapse expanded text-center">
          <div class="medium-12 columns">
              <div class="page-header-billboard">
                  <div class="inner">
                      <h2 style="font-size:40px!important; margin-top:7px!important;">OUR CAPABILITIES</h2>
                  </div>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/banner-background-gray.svg">
                  <div class="overlay" style="opacity:.2!important;"></div>
              </div>
          </div>
      </div>
  </section>
  <section class="section section-case-study section-case-study-single">
      <div class="row column"><hr class="dotted"></div>
  </section> -->


  <!--
  //////////////////////////////
  //////////////////////////////
  ////////////////////////////// CAPABILITIES CONTENT
  //////////////////////////////
  //////////////////////////////
   -->





 <!--
 //////////////////////////////
 //////////////////////////////
 ////////////////////////////// CAPABILITIES INFO GRAPHIC
 //////////////////////////////
 //////////////////////////////
  -->



<!--

  <section class="section-case-study section-case-study-single">


      <div class="row">
        <div class="small-12 medium-12 columns align-middle text-center">
            <p class="blog-text">
                  <?php the_field('capabilities_text','option'); ?>
            </p>
        </div>
      </div>
  </section> -->


  <div class="row column"><hr class="dotted"></div>



<?php get_footer(); ?>

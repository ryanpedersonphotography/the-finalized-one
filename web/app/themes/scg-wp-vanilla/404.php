<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="row">

			<main id="main" class="large-8 medium-8 columns" role="main">

				<article>

					<header class="article-header">
						<h1>Epic 404 - Article Not Found</h1>
					</header> <!-- end article header -->

					<section class="entry-content">
						<p>The article you were looking for was not found, but maybe try looking again!</p>
					</section> <!-- end article section -->

				</article> <!-- end article -->

			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>

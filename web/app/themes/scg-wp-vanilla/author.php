<?php
/*
 Template Name: Author Page
*/
?>
<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>

  <?php
    $post_id = get_the_ID();
    $post_author_id = get_post_field( 'post_author', $post_id );
    $author_url = get_avatar_url( $post_author_id );
    $user_email = get_the_author_meta( 'user_email' );


    ?>


  <section class="hero" style="background: url('<?php

the_field('employee_blog_hero_image', 'user_' . $post_author_id);

  ?>') no-repeat center center; background-size: cover;">
    <div class="row intro expanded"></div>
  </section>

<section class="section section-case-study-single">
    <div class="row">
      <div class="small-12 columns">
          <div class="row column text-center align-center"><hr class="dotted"></div>
          <div class="text-center">

            <img src='<?php echo $author_url; ?>' style='border-radius: 50%!important; width:10%;' />
          </div>
            <div class="text-center">
                <h1 class"text-center">View More Posts by <?php the_author_posts_link(); ?></h1>
            </div>
            <div class="row column"><hr class="dotted"></div>
        </div>
    </div>
</section>
<section class="section section-posts text-center">
<div class="row">
  <?php
  $post_id  = get_the_ID();
  $author   = get_the_author_meta($post_id);
  echo do_shortcode('[ajax_load_more author="'.$author.'"theme_repeater="alm-default.php" repeater="template_1" post_type="post" posts_per_page="6" transition="fade"]');
  ?>
</div>
</section>



<div class="row column"><hr /></div>
<?php endwhile; // End the loop ?>
<?php get_footer(); ?>

<?php
/*
 Template Name: Our Philosophy Layout Tuesday Night
*/
?>
<?php get_header(); ?>

<!--
//////////////////////////////
//////////////////////////////
////////////////////////////// START INTRO HEADLINE
//////////////////////////////
////////////////////////////// -->
        <?php while (have_posts()) : the_post(); ?>

<section class="hero" style="background: url('<?php the_post_thumbnail_url('large');?>') no-repeat center center;  background-size: cover;">
  <div class="row intro expanded"></div>
</section>
<?php endwhile; // End the loop ?>







<?php if ( have_rows( 'Section' ) ): ?>
	<?php while ( have_rows( 'Section' ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'intro_section' ) : ?>

<section class="section section-case-study-single">
    <div class="row">
      <div class="small-12 large-12 columns">
          <div class="row column text-center align-center"><hr class="dotted"></div>



         <h1 class="text-center">
			<?php the_sub_field( 'section_title' ); ?>
         </h1>



            <div class="text-center">
                <h4 class"text-center">
			<?php the_sub_field( 'section_sub_title' ); ?>
                </h4>
            </div>
            <div class="row column"><hr class="dotted"></div>
        </div>
    </div>
</section>


<div class="text-center show-for-small-only">
  <img src="<?php the_sub_field( 'section_image' ); ?>"  style="border-radius: 100%!important; width:50%;"/>
</div>



<section class="section-case-study section-case-study-single">
    <div class="row">
      <div class="small-12 medium-6 columns align-middle">

          <p class="blog-text text-left">

			<?php the_sub_field( 'section_text' ); ?>

          </p>
      </div>
      <div class="small-12 medium-6 columns align-middle text-center">
          <div class="row small-12 medium-6 text-center align-center">
              <div class="column small-12 medium-12 text-center align-center">
                <blockquote class="cite hide-for-small-only">
	                   <img src="<?php the_sub_field( 'section_image' ); ?>"  style="border-radius: 50%!important; width:50%;" class="hide-for-small-only" />
                </blockquote>
            </div>
          </div>
      </div>
    </div>
    <div class="row column"><hr class="dotted"></div>
</section>








		<?php elseif ( get_row_layout() == 'text_right_section' ) : ?>



<div style="margin-top:0px;margin-bottom:0px;padding-top:1em;padding-bottom:1em;">
    <label style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/banner-background-<?php the_sub_field( 'title_background_color' ); ?>.svg');
    height:auto;
    width:100vw;
    display:inline-block;
    background-size:cover;
    padding-top:1.5em;
    padding-bottom:1.5em;
    overflow:visible;box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;">
        <div style="text-align:center;width:90vw;"class="row column text-center">
<p style="text-shadow: 3px 3px 7px rgba(0,0,0, 0.62)!important;color:white!important;font-size:2.5em!important;">
<?php the_sub_field( 'section_title' ); ?>
</p></div>
    </label>
</div>



<section class="section-case-study section-case-study-single">
    <div class="row column"><hr class="dotted"></div>
    <div class="row">
      <div class="small-12 medium-6 columns align-middle text-center">
          <div class="row small-12 medium-6 text-center align-center">
              <div class="column small-12 medium-12">
                <blockquote class="cite hide-for-small-only">
                    <img src="<?php the_sub_field( 'section_image' ); ?>" style="border-radius: 50%!important; width:<?php the_sub_field( 'image_width' ); ?>%;" class="hide-for-small-only" />
                </blockquote>
            </div>
          </div>
      </div>
      <div class="text-center show-for-small-only" >
             <img src="<?php the_sub_field( 'section_image' ); ?>" style="border-radius:50%!important; width:80%;" class="show-for-small-only"/>
      </div>
      <div class="small-12 medium-6 columns align-middle">
          <p class="blog-text text-left">
                <?php the_sub_field( 'section_text' ); ?>
          </p>
      </div>
    </div>
    <div class="row column"><hr class="dotted"></div>
</section>




		<?php elseif ( get_row_layout() == 'text_left_section' ) : ?>


<div style="margin-top:0px;margin-bottom:0px;padding-top:1em;padding-bottom:1em;">
    <label style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/banner-background-<?php the_sub_field( 'title_background_color' ); ?>.svg');
    height:auto;
    width:100vw;
    display:inline-block;
    background-size:cover;
    padding-top:1.5em;
    padding-bottom:1.5em;
    overflow:visible;box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;">
        <div style="text-align:center;width:90vw;"class="row column text-center">
<p style="text-shadow: 3px 3px 7px rgba(0,0,0, 0.62)!important;color:white!important;font-size:2.5em!important;">
<?php the_sub_field( 'section_title' ); ?>
</p></div>
    </label>
</div>


<section class="section-case-study section-case-study-single">
    <div class="row column"><hr class="dotted"></div>
    <div class="row">
      <div class="text-center show-for-small-only" >
             <img src="<?php the_sub_field( 'section_image' ); ?>" style="border-radius:50%!important; width:80%;" class="show-for-small-only"/>
      </div>
      <div class="small-12 medium-6 columns align-middle">
          <p class="blog-text text-left">
                <?php the_sub_field( 'section_text' ); ?>
          </p>
      </div>
      <div class="small-12 medium-6 columns align-middle text-center">
          <div class="row small-12 medium-6 text-center align-center">
              <div class="column small-12 medium-12">
                <blockquote class="cite hide-for-small-only">
                    <img src="<?php the_sub_field( 'section_image' ); ?>" style="border-radius: 50%!important; width:<?php the_sub_field( 'image_width' ); ?>%;" class="hide-for-small-only" />
                </blockquote>
            </div>
          </div>
      </div>
    </div>
    <div class="row column"><hr class="dotted"></div>
</section>


		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>

<div style="margin-top:0px;margin-bottom:0px;padding-top:1em;padding-bottom:1em;">
    <label style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/banner-background-blue.svg');
    height:auto;
    width:100vw;
    display:inline-block;
    background-size:cover;
    padding-top:1.5em;
    padding-bottom:1.5em;
    overflow:visible;box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)!important;">
        <div style="text-align:center;width:90vw;"class="row column text-center">
<p style="text-shadow: 3px 3px 7px rgba(0,0,0, 0.62)!important;color:white!important;font-size:2.5em!important;">
Our Capabilities
</p></div>
    </label>
</div>



  <section class="section section-case-study">
      <div class="row column"><hr class="dotted"></div>
  </section>

<div class="row">
  <?php if ( have_rows( 'individual_capability_blurbs','option' ) ): ?>
    <?php while ( have_rows( 'individual_capability_blurbs','option' ) ) : the_row(); ?>
        <?php if ( get_row_layout() == 'capability' ) : ?>
           <div class="columns small-6 medium-3">
              <div class="circle-<?php the_sub_field( 'individual_capability_color','option' ); ?>">
                <div class="circle__inner">
                  <div class="circle__wrapper">
                    <div class="circle__content"><h3 class="circle-text"><?php the_sub_field( 'individual_capability','option' ); ?></h3></div>
                      </div>
                    </div>
                  </div>
                  </div>
       <?php endif; ?>
     <?php endwhile; ?>
   <?php else: ?>
   <?php endif; ?>
</div>

  <section class="section-case-study section-case-study-single">
      <div class="row">
        <div class="small-12 medium-12 columns align-middle text-center">
            <p class="blog-text">
                  <?php the_field('capabilities_text','option'); ?>
            </p>
        </div>
      </div>
  </section>

  <section class="section section-case-study section-case-study-single">
      <div class="row column"><hr class="dotted"></div>
  </section>



<?php get_footer(); ?>
